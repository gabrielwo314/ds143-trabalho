# ds143-trabalho

Trabalho final da disciplina DS143, Estrutura de Dados 2, do curso TADS.

## Como executar

1. Abra seu cmd no local do arquivo, após clonar o repositório.
2. Execute o seguinte comando para obter o .exe do programa.
    ```gcc trabalho.c -o indexer.exe```
3. Para executar o programa tenha um ou mais arquivos .txt no mesmo local em que o repositório foi clonado. Então, execute as seguintes linhas de comando:
    - ```indexer --freq N ARQUIVO``` para obter a quantidade (N) de palavras mais utilizadas no ARQUIVO passado.
    - ```indexer --freq-word PALAVRA ARQUIVO``` para obter a quantidade de vezes que tal PALAVRA apresenta-se no ARQUIVO.
    - ```indexer --search TERMO [ARQUIVO ...]``` para obter a listagem dos arquivos mais relevantes encontrados pela busca por TERMO. [ARQUIVO...] deve ser mais de um arquivo.
